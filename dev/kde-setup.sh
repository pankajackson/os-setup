#!/usr/bin/env bash

alias knshandler="/usr/lib/x86_64-linux-gnu/libexec/kf5/kpackagehandlers/knshandler"

# Folder for storing global npm packages
mkdir "${HOME}/.npm-global"
echo "  ✔️  Folder ~/.npm-packages created!"

offer_driver_installation(){
  echo "Do you want me to install Nvidia/AMD drivers? y/N"

  read answer

  if [ "$answer" == "y" ]; then
    sudo ubuntu-drivers autoinstall
    # sudo ubuntu-drivers devices - check for available drivers
    echo "  ✔️  Nvidia Driver Installed."
    echo "  ℹ️  Nvidia Driver Version: $(nvidia-settings --version | awk 'NR==2 {print $3}')"
  fi
}

offer_kubuntu_backports(){
  echo "Do you want me to add Kubuntu Backports PPA? y/N"
  read answer

  if [ "$answer" == "y" ]; then
    sudo add-apt-repository ppa:kubuntu-ppa/backports
    echo "  ✔️  Kubuntu Backports PPA added."
  fi
}

remove_default_packages(){
  echo "  ℹ️  Removing KDE's bloat..."

  sudo apt purge kate kwrite k3b skanlite muon kwalletmanager puredata partitionmanager
  echo "  ✔️  KDE default apps removed."
}

install_packages_apt(){
  echo "  ℹ️  Installing Packages with APT..."

  sudo apt install -y zsh fonts-noto powerline fonts-powerline tldr tlp qttools5-dev libqt5svg5-dev linux-tools-common linux-tools-generic linux-cloud-tools-generic ruby gem ruby-dev latte-dock neofetch kdialog screenfetch tilix gparted wget gcc python3-pip kazam figlet toilet unrar transmission tree traceroute rclone cmatrix hddtemp lm-sensors zenity grsync vim kcron ncdu curl bleachbit libnotify-bin xsel cargo cmake elementary-icon-theme deepin-icon-theme build-essential file git smartmontools yad i7z kup-backup borgbackup kde-config-cron

  echo "  ✔️  Packages installed with apt."

  # INSTALL BREW
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"

  # cpupower
  sudo apt-get install -y linux-tools-$(uname -r)

  # Install modules for colorized output
  sudo gem install colorls

  sudo cargo install exa
}

install_packages_brew(){
  echo "  ℹ️  Installing Packages with Brew..."
  brew install gcc
  brew install tokei
  brew install lsd
  brew install bench
  brew install newt # GUI framework for bash scripts
  brew install hexyl
  brew install tig
  brew install asciinema
  brew install ripgrep
  brew install fd
  brew install bat
  brew install fzf
  brew install fx
  echo "  ✔️  Packages installed with brew."
}

setup_zsh(){
  chsh -s `which zsh`
  echo "  ✔️  zsh set as default shell"

  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  echo "  ✔️  oh-my-zsh installed"

  git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
  git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions

  wget https://gist.githubusercontent.com/VladimirMikulic/39d7dad22f29189affa7ef3e1687c0dd/raw/b242f3f1ff852002f151a0cb3d0f352f2d05e83d/tilix-dev.json
  mkdir -p ~/.config/tilix/schemes
  mv tilix-dev.json ~/.config/tilix/schemes
}

fix_tilix_vte(){
  sudo ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh
}

fix_wps_mime_types(){
  sudo rm /usr/share/mime/packages/wps-office-*.xml
  sudo update-mime-database /usr/share/mime

  echo "  ℹ️ .xlsx & .docx files opening as zip error is fixed.\n Now just assign WPS as default app for these types"
}

fix_wps_missing_fonts(){
  git clone https://github.com/IamDH4/ttf-wps-fonts.git ~/Downloads/ttf-wps-fonts
  cd ~/Downloads/ttf-wps-fonts; sudo bash install.sh; rm -rf /tmp/ttf-wps-fonts
  rm ~/Downloads/ttf-wps-fonts
}

install_kde_widgets(){
  echo "\n  ℹ️  Installing Plasma widgets..."

  knshandler kns://plasmoids.knsrc/api.kde-look.org/998917
  echo "  ✔️  Weather widget installed"

  knshandler kns://plasmoids.knsrc/api.kde-look.org/998915
  echo "  ✔️  Thermal Monitor widget installed"

  knshandler kns://plasmoids.knsrc/api.kde-look.org/1245902
  echo "  ✔️  Chili Clock widget installed"

  knshandler kns://plasmoids.knsrc/api.kde-look.org/1298955
  echo "  ✔️  Plasma Customization Saver widget installed"
}

install_kde_appereance(){
  echo "  ℹ️  Installing Appearance packages..."

  knshandler kns://lookandfeel.knsrc/api.kde-look.org/1176904
  echo "  ✔️  Look & Feel Theme \"Modern\" installed."

  knshandler kns://xcursor.knsrc/api.kde-look.org/999997
  echo "  ✔️  Cursor \"Oxygen Neon\" installed."

  knshandler kns://icons.knsrc/api.kde-look.org/1148695
  echo "  ✔️  Icons \"La Capitaine\" installed."

  knshandler kns://cgcgtk3.knsrc/api.kde-look.org/1253385
  echo "  ✔️  GTK3 Theme \"Sweet\" installed."

  knshandler kns://window-decorations.knsrc/api.kde-look.org/1169286
  echo "  ✔️  Window Decoration \"Breezemite\" installed."

  knshandler kns://window-decorations.knsrc/api.kde-look.org/1254986
  echo "  ✔️  Kwin Script \"Move Window to Center\" installed."

  knshandler kns://window-decorations.knsrc/api.kde-look.org/1294604
  echo "  ✔️  Kwin Script \"Force Blur\" installed."

  knshandler kns://window-decorations.knsrc/api.kde-look.org/1112552
  echo "  ✔️  Kwin Script \"Sticky Window Snapping\" installed."

  echo "  ❌  Dolphin services cant be installed via API(knshandler).\n You'll have to install each service manually :("
  echo "  ❌  Install Latte spacer widget manually."
}

main(){
  offer_driver_installation
  offer_kubuntu_backports
  remove_default_packages
  install_packages_apt
  install_packages_brew
  setup_zsh
  fix_tilix_vte
  fix_wps_mime_types
  fix_wps_missing_fonts
  install_kde_widgets
  install_kde_appereance
}

main