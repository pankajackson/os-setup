import os
from libs import executer

# Variables Deceleration
basic_packages = ['vim', 'git', 'apt-transport-https', 'ca-certificates', 'curl']
advanced_packages = ['docker.io', 'kubectl', 'qt5-style-kvantum', 'qt5-style-kvantum-themes', 'pinta', 'yakuake', 'kde-wallpapers', 'plasma-workspace-wallpapers', 'rar', 'unrar', 'zip', 'unzip', 'okular', 'spotify-client', 'google-chrome-stable', 'net-tools', 'dnsutils', 'iputils-ping', 'keepass2', 'openvpn3', 'pritunl-client-electron', 'awscli', 'virtualbox', 'virtualbox-dkms', 'virtualbox-ext-pack']
dependent_packages = ['python3', 'python3-pip', 'python-is-python3', 'kde-full']
repos = {
    'kubernetes': {
        'key': 'sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg',
        'repo': 'echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list',
    },
    'spotify': {
        'key': 'curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add -',
        'repo': 'echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list'
    },
    'google-chrome': {
        'key': 'wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -',
        'repo': 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list'
    },
    'keepass': {
        'repo': 'sudo apt-add-repository ppa:jtaylor/keepass -y'
    },
    'openvpn3': {
        'key': 'curl -sS https://swupdate.openvpn.net/repos/openvpn-repo-pkg-key.pub | sudo apt-key add -',
        'repo': '''sudo wget -O /etc/apt/sources.list.d/openvpn3.list https://swupdate.openvpn.net/community/openvpn3/repos/openvpn3-$(env -i bash -c '. /etc/os-release; echo $VERSION_CODENAME').list'''
    },
    'openvpn-gui-client': {
        'key': 'sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A',
        'repo': '''echo "deb https://repo.pritunl.com/stable/apt $(env -i bash -c '. /etc/os-release; echo $VERSION_CODENAME') main" | sudo tee /etc/apt/sources.list.d/pritunl.list'''
    }
}
plasma_themes = {
    'base_paths': {
        'plasma-themes':  '~/.local/share/plasma/desktoptheme',
        'look-and-feel' : '~/.local/share/plasma/look-and-feel',
        'icons': '~/.local/share/icons',
        'color-schemes': '~/.local/share/color-schemes',
        'aurorae-theme': '~/.local/share/aurorae/themes',
        'kvantum': '~/.config/Kvantum',
        'kservices': '~/.local/share/kservices5'

    }
}

# Root user pipeline
power_user_pipeline = {
    'install-dependencies': {
        'apt-update': 'sudo apt-get update',
        'install-packages': {}
    },
    'enable-password-less-sudo': {
        'configure': '''echo "$(awk -F'[/:]' '{if ($3 == 1000) print $1}' /etc/passwd) ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers'''
    }
}

# Normal user pipeline
pipeline = {
    'install-basic-software': {
        'apt-update': 'sudo apt-get update',
        'apt-upgrade': 'sudo apt-get -y upgrade',
        'install-packages': {}
    },
    'setup-repos': {},
    'install-advanced-software': {
        'apt-update': 'sudo apt-get update',
        'install-packages': {}
    },
    'setup-ssh': {
        'install-packages': 'sudo apt install -y openssh-server toilet lolcat figlet fortune cowsay',
        'enable-banner': 'sudo sed -i  "/Banner\ /c\Banner\ \/etc\/issue.net" /etc/ssh/sshd_config',
        'configure-banner': [
            'toilet -f term  -w 102 "Welcome to ...." | sudo tee /etc/issue /etc/issue.net',
            'toilet -f mono12 -F border -w 500 "JACKSON.COM" | sudo tee -a /etc/issue /etc/issue.net',
            'toilet -f term  -w 102 "$(hostname)" | sudo tee -a /etc/issue /etc/issue.net',
            'toilet -f term  -w 102 "ALERT! You are entering into a secured area! Your IP, Login Time, Username has been noted and has been sent to the server administrator! This service is restricted to authorized users only. All activities on this system are logged. Unauthorized access will be fully investigated and reported to the appropriate law enforcement agencies." | sudo tee -a /etc/issue /etc/issue.net'
        ],
        'restart-service': 'sudo systemctl restart ssh'
    },
    'setup-plasma': {
        'install-python-modules': 'python -m pip install pyyaml',
        'install-plasmasaver': 'sudo install --backup -D kde-plasma/plasmasaver/binaries/plasmasaver.py /bin/plasmasaver',
        'save-default-settings': 'plasmasaver -s default',
        'export-default-settings': 'plasmasaver -e default',
        'setup-base-paths': {},
        'setup-theme-profile' : {
            'Orchis': {
                'install-kservices': {
                    'setAsWallpaper': 'cp -rvf kde-plasma/Orchis/kservices/wallpaper-changer/*.desktop %s' % plasma_themes['base_paths']['kservices']
                },
                'install-binary': {
                    'ksetwallpaper': 'sudo install --backup -D kde-plasma/Orchis/binaries/ksetwallpaper /bin/ksetwallpaper',
                    'WallpaperChanger': 'sudo install --backup -D kde-plasma/Orchis/binaries/WallpaperChanger /bin/WallpaperChanger',
                },
                'install-dependencies': {
                    'apt-packages': 'sudo apt install -y gnome-themes-standard gtk2-engines-murrine sassc'
                },
                'setup-wallpaper': {
                    'install-plugin': {
                        'inactive-blur': 'plasmapkg2 -t Plasma/Wallpaper -i kde-plasma/Orchis/wallpapers/plugins/inactiveblur-v5.zip'
                    },
                    'set-wallpaper': {
                        'wallpaper-0': 'ksetwallpaper kde-plasma/Orchis/wallpapers/images/0.png  "com.github.zren.inactiveblur" "Image"'
                    },
                },
                'setup-windows-behaviour': {
                    'configure-windows-placement': 'kwriteconfig5 --file kwinrc --group Windows --key Placement Centered'
                },
                'setup-kwinscripts': {
                    'install-force-blur': 'plasmapkg2 -t KWin/Script -i kde-plasma/Orchis/kwinscripts/forceblur-0.5.kwinscript',
                    'install-latte-window-colors': 'plasmapkg2 -t KWin/Script -i kde-plasma/Orchis/kwinscripts/window-colors-0.2.kwinscript',
                    'enable-force-blur': 'kwriteconfig5 --file kwinrc --group Plugins --key forceblurEnabled true',
                    'enable-latte-window-colors': 'kwriteconfig5 --file kwinrc --group Plugins --key lattewindowcolorsEnabled true'
                },
                'setup-desktop-effects': {
                    'configure-blur_BlurStrength': 'kwriteconfig5 --file kwinrc --group Effect-Blur --key BlurStrength 8',
                    'configure-blur_NoiseStrength': 'kwriteconfig5 --file kwinrc --group Effect-Blur --key NoiseStrength 8',
                    'enable-slide-back': 'kwriteconfig5 --file kwinrc --group Plugins --key slidebackEnabled true',
                    'enable-dialogparent': 'kwriteconfig5 --file kwinrc --group Plugins --key kwin4_effect_dialogparentEnabled true',
                    'enable-dimscreenadminmode': 'kwriteconfig5 --file kwinrc --group Plugins --key kwin4_effect_dimscreenEnabled true',
                    'enable-diminactive': 'kwriteconfig5 --file kwinrc --group Plugins --key diminactiveEnabled true',
                    'enable-scale': 'kwriteconfig5 --file kwinrc --group Plugins --key kwin4_effect_scaleEnabled true',
                },
                'setup-screen-edges': {
                    'configure-top-left-show-windows-all-desktop': 'kwriteconfig5 --file kwinrc --group TabBox --key BorderActivate 9',
                    'configure-bottom-right-show-desktop': 'kwriteconfig5 --file kwinrc --group ElectricBorders --key BottomRight ShowDesktop'
                },
                'setup-screen-locking': {
                    'configure-timeout': 'kwriteconfig5 --file kscreenlockerrc --group Daemon --key Timeout 30',
                    'configure-grace-period': 'kwriteconfig5 --file kscreenlockerrc --group Daemon --key LockGrace 10'
                },
                'setup-virtual-desktop': {
                    'configure-rows': 'kwriteconfig5 --file kwinrc --group Desktops --key Rows 1',
                    'configure-numbers': 'kwriteconfig5 --file kwinrc --group Desktops --key Number 4',
                    'desktop1': {
                        'set-id': 'kwriteconfig5 --file kwinrc --group Desktops --key Id_1 3fc48ec4-1397-442b-9d5a-55d1d44ce544',
                        'set-name': 'kwriteconfig5 --file kwinrc --group Desktops --key Name_1 Erangle',
                    },
                    'desktop2': {
                        'set-id': 'kwriteconfig5 --file kwinrc --group Desktops --key Id_2 def67170-954a-45ae-afd2-2b5db9a941ac',
                        'set-name': 'kwriteconfig5 --file kwinrc --group Desktops --key Name_2 Miramar',
                    },
                    'desktop3': {
                        'set-id': 'kwriteconfig5 --file kwinrc --group Desktops --key Id_3 17483663-c45c-4ea2-a68b-195ddd8d1da3',
                        'set-name': 'kwriteconfig5 --file kwinrc --group Desktops --key Name_3 Sanhok',
                    },
                    'desktop4': {
                        'set-id': 'kwriteconfig5 --file kwinrc --group Desktops --key Id_4 24a216f2-1326-41ae-bd0f-898e4fee69ff',
                        'set-name': 'kwriteconfig5 --file kwinrc --group Desktops --key Name_4 Vikendi',
                    },
                    'configure-slide-desktop-switcher': {
                        'set-duration': 'kwriteconfig5 --file kwinrc --group Effect-Slide --key Duration 750',
                        'set-horizontal-gap': 'kwriteconfig5 --file kwinrc --group Effect-Slide --key HorizontalGap 30',
                        'set-vertical-gap': 'kwriteconfig5 --file kwinrc --group Effect-Slide --key VerticalGap 30',
                    },
                },
                'setup-krunner-search': {
                    'set-screen-position': 'kwriteconfig5 --file krunnerrc --group General --key FreeFloating true',
                },
                'setup-display-and-monitor': {
                    'set-compositing-rendering-backend-opengl': 'kwriteconfig5 --file kwinrc --group Compositing --key Backend OpenGL',
                    'set-compositing-rendering-enabled': 'kwriteconfig5 --file kwinrc --group Compositing --key Enabled true',
                    'set-compositing-rendering-backend-opengl3.1': 'kwriteconfig5 --file kwinrc --group Compositing --key GLCore true',
                    'set-compositing-GLPreferBufferSwap': 'kwriteconfig5 --file kwinrc --group Compositing --key GLPreferBufferSwap a',
                    'set-compositing-scale-mode-smooth': 'kwriteconfig5 --file kwinrc --group Compositing --key GLTextureFilter 1',
                    'set-compositing-HiddenPreviews': 'kwriteconfig5 --file kwinrc --group Compositing --key HiddenPreviews 5',
                    'set-compositing-OpenGLIsUnsafe': 'kwriteconfig5 --file kwinrc --group Compositing --key OpenGLIsUnsafe false',
                    'set-compositing-WindowsBlockCompositing': 'kwriteconfig5 --file kwinrc --group Compositing --key WindowsBlockCompositing true',
                    'set-compositing-XRenderSmoothScale': 'kwriteconfig5 --file kwinrc --group Compositing --key XRenderSmoothScale false',
                },
                'setup-splash-screen': {
                    'set-engine-none': 'kwriteconfig5 --file ksplashrc --group KSplash --key Engine none',
                    'set-theme-none': 'kwriteconfig5 --file ksplashrc --group KSplash --key Theme None',
                },
                #TODO: setup window Decoration Titlebar Buttons
                'setup-theme': {
                    'install-kde-themes': 'bash kde-plasma/Orchis/global-themes/install.sh',
                    'install-gtk-theme': 'bash kde-plasma/Orchis/gtk-themes/install.sh'
                },
                'setup-icons-theme': {
                    'install-tela-circle-icon-theme': 'bash kde-plasma/Orchis/icons/tela-circle-icon-theme/install.sh -a'
                },
                'setup-cursors-theme': {
                    'install-vimix-theme': 'sudo kde-plasma/Orchis/cursors/vimix-cursors/install.sh'
                },

                'save-Orchis-theme': 'plasmasaver -s Orchis',
            }
        }
    }
}

for pkg in dependent_packages:
    power_user_pipeline['install-dependencies']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for pkg in basic_packages:
    pipeline['install-basic-software']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for pkg in advanced_packages:
    pipeline['install-advanced-software']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for repo in repos.keys():
    pipeline['setup-repos'][repo] = {}
    if 'key' in repos[repo].keys():
        pipeline['setup-repos'][repo]['add-key'] = repos[repo]['key']
    if 'repo' in repos[repo].keys():
        pipeline['setup-repos'][repo]['add-repo'] = repos[repo]['repo']
for path in plasma_themes['base_paths'].keys():
    pipeline['setup-plasma']['setup-base-paths'][path] = 'mkdir -p %s' % plasma_themes['base_paths'][path]
    
if os.getuid() == 0:
    executer.execute(power_user_pipeline)
else:
    executer.execute(pipeline)