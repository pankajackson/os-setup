import subprocess
import sys
import os
import threading
import time
import unicodedata
import json


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Loader(threading.Thread):
    def __init__(self, title='', commands=None):
        self.out = sys.stdout
        self.flag = False
        self.title = '\r' + title
        self.string = ''
        self.waittime = 0.2
        self.ts = time.time()
        self.temp_file = '/tmp/loader%s.log' % self.ts
        self.default_ind_blk = 2
        self.default_title = 'Task'
        self.commands = commands
        if os.name == 'posix':
            self.spinchars = (unicodedata.lookup('FIGURE DASH') + u' ', u'\\ ', u'| ', u'/ ')
        else:
            self.spinchars = (u'--', u'\\ ', u'| ', u'/ ')
        threading.Thread.__init__(self, None, None, "Spin Thread")

    def get_log(self):
        logs = None
        with open(self.temp_file, 'r') as file:
            logs = file.read()
        return logs

    def spin(self):
        for x in self.spinchars:
            self.string = self.title + " ... " + x
            self.out.write(self.string)
            self.out.flush()
            time.sleep(self.waittime)

    def run(self):
        while not self.flag:
            self.spin()

    def stop(self, status):
        self.flag = True
        self.out.flush()
        time.sleep(1)
        result = ''
        if status == 1:
            result = bcolors.FAIL + "Failed!" + bcolors.ENDC
        elif status == 0:
            result = bcolors.OKGREEN + "Done!" + bcolors.ENDC
        elif status == 2:
            result = bcolors.WARNING + 'Skipped!' + bcolors.ENDC
        else:
            result = result
        print(self.title + " ... " + result)

# --------------------------------------------------

def exec_cmd(command, title):
    if command:
        if not title:
            title = 'Task'
        load = Loader(title=bcolors.OKBLUE + title + bcolors.ENDC)
        load.start()
        if int(subprocess.check_output("%s 1>%s 2>>%s; echo $?" % (command, load.temp_file, load.temp_file), shell=True)) != 0:
            load.stop(1)
            print(bcolors.FAIL + load.get_log() + bcolors.ENDC)
        else:
            load.stop(0)

def evaluate_commands(commands, ind_blk=0):
    default_ind_blk = 2
    ind_blk_spaces = ind_blk*default_ind_blk
    try:
        commands = json.loads(commands)
        evaluate_commands(commands)
    except:
        pass
    if isinstance(commands, dict):
        for cmd_titles in commands.keys():
            ind_blk += 1
            if isinstance(commands[cmd_titles], str):
                exec_cmd(command=commands[cmd_titles], title=' '*ind_blk_spaces + cmd_titles)
            else:
                print(' '*ind_blk_spaces + bcolors.BOLD + "[" + cmd_titles + "]" + bcolors.ENDC)
                evaluate_commands(commands=commands[cmd_titles], ind_blk=ind_blk)
            ind_blk -= 1
    elif isinstance(commands, tuple) or isinstance(commands, list):
        for cmd in commands:
            ind_blk += 1
            if isinstance(cmd, str):
                exec_cmd(command=cmd, title=' '*ind_blk_spaces + str(cmd))
            else:
                print(' '*ind_blk_spaces + bcolors.BOLD + "[" + cmd + "]" + bcolors.ENDC)
                evaluate_commands(commands=cmd, ind_blk=ind_blk)
            ind_blk -= 1
    elif isinstance(commands, str):
        exec_cmd(command=commands, title=' '*ind_blk_spaces + commands)
    else:
        print(bcolors.FAIL + 'Error: arg commands type mismatch: expected str, list, dict, json but got %s' % (type(commands)) + bcolors.ENDC)

def execute(commands):
    evaluate_commands(commands)