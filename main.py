import os
from libs import executer

# Variables Deceleration
basic_packages = ['vim', 'git', 'apt-transport-https', 'ca-certificates', 'curl']
advanced_packages = ['docker.io', 'kubectl', 'pinta', 'yakuake', 'plasma-workspace-wallpapers', 'rar', 'unrar', 'zip', 'unzip', 'okular', 'spotify-client', 'google-chrome-stable', 'net-tools', 'dnsutils', 'iputils-ping', 'keepass2', 'openvpn3', 'pritunl-client-electron', 'awscli', 'virtualbox', 'virtualbox-dkms', 'virtualbox-ext-pack']
dependent_packages = ['python3', 'python3-pip', 'python-is-python3', 'kde-full']
plasma_dependent_packages = {
    'Orchis': ['cmake', 'extra-cmake-modules', 'g++', 'qtbase5-dev', 'qtdeclarative5-dev', 'libqt5x11extras5-dev', 'libkf5plasma-dev', 'libkf5globalaccel-dev', 'libkf5xmlgui-dev', 'qt5-style-kvantum', 'qt5-style-kvantum-themes', 'gnome-themes-standard', 'gtk2-engines-murrine', 'sassc', 'latte-dock', 'conky-all', 'zsh']
}
repos = {
    'kubernetes': {
        'key': 'sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg',
        'repo': 'echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list',
    },
    'spotify': {
        'key': 'curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add -',
        'repo': 'echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list'
    },
    'google-chrome': {
        'key': 'wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -',
        'repo': 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list'
    },
    'keepass': {
        'repo': 'sudo apt-add-repository ppa:jtaylor/keepass -y'
    },
    'openvpn3': {
        'key': 'curl -sS https://swupdate.openvpn.net/repos/openvpn-repo-pkg-key.pub | sudo apt-key add -',
        'repo': '''sudo wget -O /etc/apt/sources.list.d/openvpn3.list https://swupdate.openvpn.net/community/openvpn3/repos/openvpn3-$(env -i bash -c '. /etc/os-release; echo $VERSION_CODENAME').list'''
    },
    'openvpn-gui-client': {
        'key': 'sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A',
        'repo': '''echo "deb https://repo.pritunl.com/stable/apt $(env -i bash -c '. /etc/os-release; echo $VERSION_CODENAME') main" | sudo tee /etc/apt/sources.list.d/pritunl.list'''
    }
}
plasma_themes = {
    'base_paths': {
        'plasma-themes':  '~/.local/share/plasma/desktoptheme',
        'look-and-feel' : '~/.local/share/plasma/look-and-feel',
        'icons': '~/.local/share/icons',
        'color-schemes': '~/.local/share/color-schemes',
        'aurorae-theme': '~/.local/share/aurorae/themes',
        'kvantum': '~/.config/Kvantum',
        'kservices': '~/.local/share/kservices5'

    }
}

# Root user pipeline
power_user_pipeline = {
    'install-dependencies': {
        'apt-update': 'sudo apt-get update',
        'install-packages': {}
    },
    'enable-password-less-sudo': {
        'configure': '''echo "$(awk -F'[/:]' '{if ($3 == 1000) print $1}' /etc/passwd) ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers'''
    }
}

# Normal user pipeline
pipeline = {
    'install-basic-software': {
        'apt-update': 'sudo apt-get update',
        'apt-upgrade': 'sudo apt-get -y upgrade',
        'set-devconf-for-interactive-virtualbox-ext-pack': 'echo virtualbox-ext-pack virtualbox-ext-pack/license select true | sudo debconf-set-selections',
        'install-packages': {}
    },
    'setup-repos': {},
    'install-advanced-software': {
        'apt-update': 'sudo apt-get update',
        'install-packages': {}
    },
    'setup-ssh': {
        'install-packages': 'sudo apt install -y openssh-server toilet lolcat figlet fortune cowsay',
        'enable-banner': 'sudo sed -i  "/Banner\ /c\Banner\ \/etc\/issue.net" /etc/ssh/sshd_config',
        'configure-banner': [
            'toilet -f term  -w 102 "Welcome to ...." | sudo tee /etc/issue /etc/issue.net',
            'toilet -f mono12 -F border -w 500 "JACKSON.COM" | sudo tee -a /etc/issue /etc/issue.net',
            'toilet -f term  -w 102 "$(hostname)" | sudo tee -a /etc/issue /etc/issue.net',
            'toilet -f term  -w 102 "ALERT! You are entering into a secured area! Your IP, Login Time, Username has been noted and has been sent to the server administrator! This service is restricted to authorized users only. All activities on this system are logged. Unauthorized access will be fully investigated and reported to the appropriate law enforcement agencies." | sudo tee -a /etc/issue /etc/issue.net'
        ],
        'restart-service': 'sudo systemctl restart ssh'
    },
    'setup-plasma': {
        'install-python-modules': 'python -m pip install pyyaml',
        'install-plasmasaver': 'sudo install --backup -D libs/plasmasaver.py /usr/bin/plasmasaver',
        'save-default-theme-profile': 'plasmasaver save default',
        'setup-theme-profile' : {
            'Orchis': {
                'install-dependencies': {
                    'install-packages': {}
                },
                'install-binary': {
                    'ksetwallpaper': 'sudo install --backup -D kde-plasma/binaries/ksetwallpaper /bin/ksetwallpaper',
                    'WallpaperChanger': 'sudo install --backup -D kde-plasma/binaries/WallpaperChanger /bin/WallpaperChanger',
                },
                'install-kservices': {
                    'setAsWallpaper': 'cp -rvf kde-plasma/kservices/wallpaper-changer/*.desktop %s' % plasma_themes['base_paths']['kservices']
                },
                'install-kwinscripts': {
                    'force-blur': 'plasmapkg2 -t KWin/Script -i kde-plasma/kwinscripts/forceblur-0.5.kwinscript',
                    'latte-window-colors': 'plasmapkg2 -t KWin/Script -i kde-plasma/kwinscripts/window-colors-0.2.kwinscript',
                },
                'install-plugin': {
                    'wallpaper-inactive-blur': 'plasmapkg2 -t Plasma/Wallpaper -i kde-plasma/plugins/wallpaper/inactiveblur-v5.zip'
                },
                'import-plasma-profile': {
                    'Orchis': 'plasmasaver import kde-plasma/profiles/Orchis/Orchis-with-sddm-local.plsv -p DUMMY_PASSWORD'
                },
                'apply-plasma-profile': {
                    'Orchis': 'plasmasaver apply Orchis-with-sddm-local -p DUMMY_PASSWORD'
                },
                'post-installation-setup': {
                    'install-virtual-desktop-bar': 'cd kde-plasma/others/virtual_desktop_bar; chmod +x scripts/* ./scripts/install-dependencies-ubuntu.sh; ./scripts/install-applet.sh',
                    'setup-conky-startup-script': 'chmod +x ~/.conky/Antares/start_conky.sh'
                },
            }
        }
    }
}

for pkg in dependent_packages:
    power_user_pipeline['install-dependencies']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for pkg in basic_packages:
    pipeline['install-basic-software']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for pkg in advanced_packages:
    pipeline['install-advanced-software']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for theme in plasma_dependent_packages.keys():
    for pkg in plasma_dependent_packages[theme]:
        pipeline['setup-plasma']['setup-theme-profile'][theme]['install-dependencies']['install-packages'][pkg] = 'sudo apt-get install -y %s' % pkg
for repo in repos.keys():
    pipeline['setup-repos'][repo] = {}
    if 'key' in repos[repo].keys():
        pipeline['setup-repos'][repo]['add-key'] = repos[repo]['key']
    if 'repo' in repos[repo].keys():
        pipeline['setup-repos'][repo]['add-repo'] = repos[repo]['repo']
    
if os.getuid() == 0:
    executer.execute(power_user_pipeline)
else:
    executer.execute(pipeline)