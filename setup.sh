#!/usr/bin/env bash
set -u

BASEDIR=$(dirname "$0")
cd $BASEDIR

# If running from non root user
if [ "$EUID" -ne 0 ]
then
  	# Start Power User Pipeline
	sudo -- sh -c "python main.py || python3 main.py || python2 main.py"

	# Start UserPipeline
	python main.py || python3 main.py || python2 main.py
else
	# Start Power User Pipeline
	python main.py || python3 main.py || python2 main.py

  	# Start UserPipeline
	sudo -u $(id -nu 1000) sh -c "python main.py || python3 main.py || python2 main.py"
fi
